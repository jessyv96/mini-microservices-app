/* COMMENTS */
const COMMENT_CREATED = 'CommentCreated'
const COMMENT_MODERATED = 'CommentModerated'
const COMMENT_UPDATED = 'CommentUpdated'

module.exports = {
    COMMENT_CREATED,
    COMMENT_MODERATED,
    COMMENT_UPDATED
} 