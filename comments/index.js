const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const axios = require('axios')
const { randomBytes } = require('crypto')
const { COMMENT_CREATED, COMMENT_UPDATED, COMMENT_MODERATED } = require('./constants')

const app = express()
app.use(bodyParser.json())
app.use(cors())

const commentByPostId = {}

app.get('/posts/:id/comments', (req, res) => {
    res.json(commentByPostId[req.params.id] || [])
})

app.post('/posts/:id/comments', async (req, res) => {
    const commentId = randomBytes(4).toString('hex')
    const { content } = req.body

    const comments = commentByPostId[req.params.id] || []
    comments.push({ id: commentId, content, status: 'pending' })
    commentByPostId[req.params.id] = comments


    await axios.post('http://localhost:4005/events', {
        type: COMMENT_CREATED,
        data: {
            id: commentId,
            content,
            postId: req.params.id,
            status: 'pending'
        }
    })

    res.status(201).json(comments)
})

//reponse de l'event
app.post('/events', async (req, res) => {
    const { type, data } = req.body

    if (type === COMMENT_MODERATED) {
        const { postId, id, status, content } = data
        const comments = commentByPostId[postId]
        const comment = comments.find(c => c.id === id)

        comment.status = status

        await axios.post('http://localhost:4005/events', {
            type: COMMENT_UPDATED,
            data: {
                id,
                postId,
                status,
                content
            }
        })
    }

    res.send({})
})

app.listen(4001, () => {
    console.log('Comment service running on port 4001')
})